# Cayden Quest

Un jeu dont vous êtes le héros.

# Structure du projet
## app
Serveur API permettant d'effectuer des actions sur la BDD.

## front
Interface WEB en React s'interfaçant avec l'API.

## bdd
Scripts SQL permetant de mettre en place la BDD et de la peupler avec les sections du livre.

# Mise en place du projet
- `git clone`
- Créer un fichier `.env` dans `app/` avec :
    ```
    DB_USERNAME='' # user postgres
    DB_PASSWORD='' # mdp postgres
    DB_CONTAINER='bdd' # ip ou nom de l'hote bdd
    DB_PORT='5432'
    DB_NAME='cayden'
    TOKEN='' # chaine servant à générer les tokens d'auth
    ```
- Mettre à jour la constante `host` dans `front/index.js` en fonction de l'ip hébergeant l'API
- Créer le dossier `bdd/secrets/` et y ajouter deux fichiers : `postgres-password` et `postgres-user`. Ensuite ajouter seulement le mot de passe et le nom d'utilisateur de la BDD dans leurs fichiers respectifs.
- `docker compose up -d`