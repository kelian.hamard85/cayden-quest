import express from "express";
import { levenshtein } from '../controllers/levenshteinControllers.js';

const routerLevenshtein = express.Router()

routerLevenshtein.post("/levenshtein", levenshtein);

export { routerLevenshtein }