import express from "express";
import { recupSection } from '../controllers/sectionControllers.js';

const routerSection = express.Router()

routerSection.post("/recupSection", recupSection)

export { routerSection }