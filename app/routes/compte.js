import express from "express";
import { infosCompte, changerEmail, changerMdp } from "../controllers/compteControllers.js"

const routerCompte = express.Router()

// malgré les aparences ce script est très utile puisqu'il permet de stocker les routes d'api relatives aux comptes qui doivent être identifier par token
routerCompte.post("/infosCompte", infosCompte)
routerCompte.post("/changerEmail", changerEmail)
routerCompte.post("/changerMdp", changerMdp)

export { routerCompte }