import { Section } from "../models/section.js"

function recupSection(_req, _res) {
    Section.recupSection(_req.body.idSection, (error, section) => {
        if (error) {
            _res.status(500).send("" + error)
        } else {
            _res.json(section)
        }
    })
}

export {
    recupSection
}