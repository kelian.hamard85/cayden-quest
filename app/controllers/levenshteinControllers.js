import { Enigme } from "../models/levenshtein.js";

function minimum(nb1, nb2, nb3) {
    return Math.min(nb1, nb2, nb3)
}

function levenshteinCalc(mot, motReponse) {
    let dist = []
    let i = 0
    let j = 0
    let cost = 0

    for (i = 0; i <= mot.length; i++) {
        dist[i] = []
        for (j = 0; j <= motReponse.length; j++) {
            if (i === 0) {
                dist[i][j] = j
            } else if (j === 0) {
                dist[i][j] = i
            } else {
                dist[i][j] = 0
            }
        }
    }

    for (i = 1; i <= mot.length; i++) {
        for (j = 1; j <= motReponse.length; j++) {
            if (mot[i-1] === motReponse[j-1]) {
                cost = 0
            } else {
                cost = 1
            }
            dist[i][j] = minimum(dist[i - 1][j] + 1, dist[i][j - 1] + 1, dist[i - 1][j - 1] + cost)
        }
    }
    
    return dist[mot.length][motReponse.length]
}

function levenshtein(_req, _res) {
    Enigme.recupRepEnigme(_req.body.idSection, (error, repEnigme) => {
        if (error) {
            _res.status(500).send("" + error)
        } else {
            // implementer levenshtein
            _res.json(levenshteinCalc(_req.body.mot, repEnigme["reponse_enigme"]))
        }
    })
}

export {
    levenshtein
}