import { Compte } from "../models/compte.js";
import bcryptjs from 'bcryptjs'
import jwt from "jsonwebtoken"

function genToken(idCompte, callback) {
    jwt.sign({id: idCompte}, process.env.TOKEN, { expiresIn: "48h" }, (err, token) => {
        callback(undefined, token)
    })
}

function inscription(_req, _res) {
    let mail = _req.body.mail
    bcryptjs.hash(_req.body.mdp, 10, (err, hash) => {
        let mdpHashe = hash
        Compte.creerCompte(mail, mdpHashe, (err, idCompte) => {
            if (err) {
                if (err == 1) {
                    return _res.status(409).send("Email déjà présent")
                } else {
                    return _res.status(500).send("" + err)
                }
            } else {
                genToken(idCompte, (err, token) => {
                    return _res.json({token, idCompte})
                })
            }
        })
    })
}

function connexion(_req, _res) {
    let mail = _req.body.mail
    let mdp = _req.body.mdp
    Compte.connexion(mail, mdp, (err, success, idCompte) => {
        if (err) {
            return _res.status(500).send("Erreur : " + err)
        } else {
            if (success) {
                genToken(idCompte, (err, token) => {
                    return _res.json({token, idCompte})
                })
            } else {
                return _res.status(401).json("Mauvais email ou mot de passe.")
            }
        }
    })
}

function infosCompte(_req, _res) {
    Compte.infosCompte(_req.body.idCompte, (err, res) => {
        if (err) {
            return _res.status(500).send("Erreur : " + err)
        } else {
            if (res == 0) {
                return _res.status(404).json("Le compte avec cet id n'a pas été trouvé.")
            } else {
                return _res.json(res)
            }
        }
    })
}

function changerEmail(_req, _res) {
    Compte.changerEmail(_req.body.idCompte, _req.body.valeur, (err, res) => {
        if (err) {
            if (err == 1) {
                return _res.status(409).send("Nom de compte déjà enregistré")
            } else {
                return _res.status(500).send("Erreur : " + err)
            }
        } else {
            if (res > 0) {
                return _res.json(res)
            } else {
                return _res.status(401).send("Erreur lors de la mise à jour")
            }
        }
    })
}

function changerMdp(_req, _res) {
    bcryptjs.hash(_req.body.valeur, 10, (err, hash) => {
        let mdpHashe = hash
        Compte.changerMdp(_req.body.idCompte, mdpHashe, (err, res) => {
            if (err) {
                return _res.status(500).send("" + err)
            } else {
                return _res.json(res)
            }
        })
    })
}

export {
    inscription,
    connexion,
    infosCompte,
    changerEmail,
    changerMdp
}