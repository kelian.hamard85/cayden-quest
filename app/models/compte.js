import { DB } from "../db.js";
import bcryptjs from 'bcryptjs'

export class Compte {
    static creerCompte(mail, mdp, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, result) => {
                if (error) {
                    client.release();
                    callback(error)
                }
                if (result.rows.length == 0) {
                    let requeteAjoutCompte = `INSERT INTO cayden_quest._compte (email, mot_de_passe) VALUES ('${mail}', '${mdp}')`;
                    DB.query(requeteAjoutCompte, (err, resultInsert) => {
                        if (err) {
                            client.release();
                            callback(err)
                        } else {
                            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, resultIdInsert) => {
                                client.release();
                                if (error) {
                                    callback(error)
                                } else {
                                    callback(undefined, resultIdInsert.rows[0].id)
                                }
                            })
                        }
                    })
                } else {
                    client.release();
                    callback(1)
                }
            })
        })
    }

    static connexion(mail, mdp, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT * FROM cayden_quest._compte where email = '${mail}'`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                }
                if (result.rows.length == 0) {
                    callback(undefined, 0)
                    return
                }
                bcryptjs.compare(mdp, result.rows[0].mot_de_passe, (err, success) => {
                    if (err) {
                        callback(err)
                    }
                    callback(undefined, success, result.rows[0].id)
                })
            })
        })
    }

    static infosCompte(idCompte, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT id, email FROM cayden_quest._compte where id = '${idCompte}'`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                }
                if (result.rows.length == 0) {
                    callback(undefined, 0)
                    return
                }
                callback(undefined, result.rows[0])
            })
        })
    }

    static changerEmail(idCompte, email, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT * FROM cayden_quest._compte where email = '${email}'`, (error, result) => {
                if (error) {
                    client.release();
                    callback(error)
                }
                if (result.rows.length == 0) {
                    DB.query(`UPDATE cayden_quest._compte SET email='${email}' WHERE id=${idCompte}`, (error, result) => {
                        client.release();
                        if (error) {
                            callback(error)
                            return
                        }
                        callback(undefined, result.rowCount)
                    })
                } else {
                    client.release();
                    callback(1)
                }
            })
        })
    }

    static changerMdp(idCompte, mdp, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`UPDATE cayden_quest._compte SET mot_de_passe='${mdp}' WHERE id=${idCompte}`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                    return
                }
                callback(undefined, result.rowCount)
            })
        })
    }
}