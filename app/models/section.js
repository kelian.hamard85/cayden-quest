import { DB } from "../db.js";

export class Section {
    static recupSection(idSection, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT id, texte, type_action, destinations FROM cayden_quest._section where id = '${idSection}'`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                    return ;
                }
                if (result.rows.length == 1) {
                    let section = result.rows[0]
                    if (section.type_action !== "fin de partie"){
                        section.destinations = JSON.parse(section.destinations)
                    }
                    callback(undefined, section)
                }
            })
        })
    }
}