import { DB } from "../db.js";

export class Enigme {
    static recupRepEnigme(idSection, callback) {
        DB.connect((err, client) => {
            if (err) {
                callback(err)
            }
            DB.query(`SELECT type_action, reponse_enigme FROM cayden_quest._section where id = '${idSection}'`, (error, result) => {
                client.release();
                if (error) {
                    callback(error)
                }
                if (result.rows.length == 1) {
                    if (result.rows[0].type_action == "enigme") {
                        callback(undefined, result.rows[0])
                    } else {
                        callback(new Error("Pas une enigme"))
                    }
                } else {
                    callback(new Error("Pas des section trouvées avec cet ID"))
                }
            })
        })
    }
}