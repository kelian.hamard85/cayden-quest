import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser"
import helmet from 'helmet';
import * as dotenv from "dotenv";
import jwt from "jsonwebtoken"
import { routerCompte } from "./routes/compte.js";
import { connexion, inscription } from "./controllers/compteControllers.js";
import { routerSection } from "./routes/section.js";
import { routerPersonnage } from "./routes/personnage.js";
import { routerLevenshtein } from "./routes/levenshtein.js";

dotenv.config()
const app = express();
// Mettre de la sécurité pour éviter tout hack sur l'API
app.use(helmet.frameguard({ action: "sameorigin" })); // peut être qu'il faudra remettre cette ligne un jour mais elle donne une cors en localhost
app.use(cookieParser());
// Renvoie les données récupérer en JSON
app.use(express.json())

// CORS
app.use(cors({
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}))
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    next()
})

// route indépendante du midleware d'auth
app.post("/compte/connexion", connexion)
app.post("/compte/inscription", inscription);

// verification des token
app.use((req, res, next) => {
    const token = req.headers['authorization']

    if (token == null) {
        return res.sendStatus(401)
    }

    jwt.verify(token, process.env.TOKEN, (err, user) => {
        if (err) {
            return res.sendStatus(403)
        }
        res.locals.user = user
        res.locals.userId = user.id
        next()
    })
})

// Mettre les routes ici
// app.use("/recupererPersonne", routerPersonne);
app.get("/tokenValide", (req, res) => {
    res.json({"(👉ﾟヮﾟ)👉": res.locals.userId})
})
app.use("/compte", routerCompte)
app.use("/personnage", routerPersonnage)
app.use("/section", routerSection)
app.use("/enigme", routerLevenshtein)
app.get("/test", (req, res) => {
    return res.json("r")
})

const PORT = 3200
app.listen(PORT)