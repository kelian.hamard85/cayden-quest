// const { Pool } = require('pg');
import pkg from 'pg';
const { Pool } = pkg;
import * as dotenv from "dotenv";

dotenv.config()

const connectionString = `postgresql://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_CONTAINER}:${process.env.DB_PORT}/${process.env.DB_NAME}`

export const DB = new Pool({
    connectionString
})